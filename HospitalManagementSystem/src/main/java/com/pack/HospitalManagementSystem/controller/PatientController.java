package com.pack.HospitalManagementSystem.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pack.HospitalManagementSystem.dao.PatientRepository;
import com.pack.HospitalManagementSystem.model.Patient;
import com.pack.HospitalManagementSystem.model.Physician;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/hospital/patient")
public class PatientController {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PatientController.class);
	@Autowired
	PatientRepository patientRepository;

	@PostMapping(value = "/enroll")
	public ResponseEntity<Patient> savePatient(@RequestBody Patient patient) {
		try {
			Patient patient1 = patientRepository.save(new Patient(patient
					.getFname(), patient.getLname(), patient.getPassword(),
					patient.getDob(), patient.getEmail(),
					patient.getPhNumber(), patient.getState(), patient
							.getInsPlan()));
			LOGGER.info("Patient Details Added");
			return new ResponseEntity<>(patient1, HttpStatus.CREATED);
		} catch (Exception e) {
			LOGGER.info("Exception in Save patient");
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping(value = "/list")
	public ResponseEntity<List<Patient>> getAllPatients() {
		List<Patient> patients = new ArrayList<Patient>();
		try {
			patientRepository.findAll().forEach(patients::add);
			if (patients.isEmpty()) {
				LOGGER.info("Empty Patient Details");
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			LOGGER.info("Patient Details Found");
			return new ResponseEntity<>(patients, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.info("Exception in Get All Patients");
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
