package com.pack.HospitalManagementSystem.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pack.HospitalManagementSystem.dao.PatientDiagnosisRepository;
import com.pack.HospitalManagementSystem.model.Patient;
import com.pack.HospitalManagementSystem.model.PatientDiagnosis;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/hospital/patientDiagnosis")
public class PatientDiagnosisController {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PatientDiagnosisController.class);
	@Autowired
	PatientDiagnosisRepository patientDiagnosisRepository;

	@PostMapping(value = "/addDetails")
	public ResponseEntity<PatientDiagnosis> addPatientDetails(
			@RequestBody PatientDiagnosis patientDiagnosis) {
		try {
			PatientDiagnosis patientDiagnosis1 = patientDiagnosisRepository
					.save(new PatientDiagnosis(patientDiagnosis.getSymptoms(),
							patientDiagnosis.getDiagnosisProvided(),
							patientDiagnosis.getAdministeredBy(),
							patientDiagnosis.getDateOfDiagnosis(),
							patientDiagnosis.getIsFollowUp(), patientDiagnosis
									.getDateOfFollowup(), patientDiagnosis
									.getBillAmount(), patientDiagnosis
									.getCardNumber(), patientDiagnosis
									.getPatientId(), patientDiagnosis.getMode()));
			LOGGER.info("Patient Diagnosis Details Added");
			return new ResponseEntity<>(patientDiagnosis1, HttpStatus.CREATED);
		} catch (Exception e) {
			LOGGER.info("Exception in add Patient Diagnosis Details");
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
}
