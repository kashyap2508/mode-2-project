package com.pack.HospitalManagementSystem.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pack.HospitalManagementSystem.dao.PhysicianRepository;
import com.pack.HospitalManagementSystem.model.Physician;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/hospital/physician")
public class PhysicianController {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PhysicianController.class);
	@Autowired
	PhysicianRepository physicianRepository;

	Random random = new Random();

	@PostMapping(value = "/add")
	public ResponseEntity<Physician> addPhysician(
			@RequestBody Physician physician) {
		try {
			int rand_int = random.nextInt(1000);
			String phyId = "PR" + rand_int;
			System.out.println(phyId);
			physician.setId(phyId);
			Physician physician1 = physicianRepository.save(new Physician(
					physician.getId(), physician.getFname(), physician
							.getLname(), physician.getDepartment(), physician
							.getEducation(), physician.getExperience(),
					physician.getState(), physician.getInsPlan()));
			LOGGER.info("Physician Details Added");
			return new ResponseEntity<>(physician1, HttpStatus.CREATED);
		} catch (Exception e) {
			LOGGER.info("Exception in adding Physician Details");
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping(value = "/search/state/{state}")
	public ResponseEntity<List<Physician>> searchByState(
			@PathVariable("state") String state) {
		try {
			List<Physician> physicians = physicianRepository.findByState(state);
			if (physicians.isEmpty()) {
				LOGGER.info("Empty Physician from that state");
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			LOGGER.info("Physician Details from that State Found");
			return new ResponseEntity<>(physicians, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.info("Exception in Physician Search BY State");
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping(value = "/search/department/{department}")
	public ResponseEntity<List<Physician>> searchByDepartment(
			@PathVariable("department") String department) {
		try {
			List<Physician> physicians = physicianRepository
					.findByDepartment(department);
			if (physicians.isEmpty()) {
				LOGGER.info("Empty Physician from that Department");
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			LOGGER.info("Physician Details from that Department Found");
			return new ResponseEntity<>(physicians, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.info("Exception in Physician Search BY State");
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping(value = "/list")
	public ResponseEntity<List<Physician>> getAllPhysicians() {
		List<Physician> physicians = new ArrayList<Physician>();
		try {
			physicianRepository.findAll().forEach(physicians::add);
			if (physicians.isEmpty()) {
				LOGGER.info("Empty Physician Details");
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			LOGGER.info("Physician Details Found");
			return new ResponseEntity<>(physicians, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.info("Exception in Get all Physician Details");
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
