package com.pack.HospitalManagementSystem.dao;

import org.springframework.data.repository.CrudRepository;

import com.pack.HospitalManagementSystem.model.PatientDiagnosis;

public interface PatientDiagnosisRepository extends
		CrudRepository<PatientDiagnosis, Integer> {

}
