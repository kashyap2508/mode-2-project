package com.pack.HospitalManagementSystem.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.pack.HospitalManagementSystem.model.Physician;

public interface PhysicianRepository extends CrudRepository<Physician, String> {
	List<Physician> findByDepartment(String department);

	List<Physician> findByState(String state);
}
