package com.pack.HospitalManagementSystem.dao;

import org.springframework.data.repository.CrudRepository;

import com.pack.HospitalManagementSystem.model.Patient;

public interface PatientRepository extends CrudRepository<Patient, Integer> {

}
