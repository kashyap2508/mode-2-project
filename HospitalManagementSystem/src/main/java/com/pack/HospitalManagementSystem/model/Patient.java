package com.pack.HospitalManagementSystem.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "patient")
public class Patient {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String fname;
	private String lname;
	private String password;
	private Date dob;
	private String email;
	private int PhNumber;
	private String state;
	private String insPlan;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPhNumber() {
		return PhNumber;
	}

	public void setPhNumber(int phNumber) {
		this.PhNumber = phNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getInsPlan() {
		return insPlan;
	}

	public void setInsPlan(String insPlan) {
		this.insPlan = insPlan;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", fname=" + fname + ", lname=" + lname
				+ ", password=" + password + ", dob=" + dob + ", email="
				+ email + ", PhNumber=" + PhNumber + ", state=" + state
				+ ", insPlan=" + insPlan + "]";
	}

	public Patient(int id, String fname, String lname, String password,
			Date dob, String email, int phNumber, String state, String insPlan) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.password = password;
		this.dob = dob;
		this.email = email;
		this.PhNumber = phNumber;
		this.state = state;
		this.insPlan = insPlan;
	}

	public Patient(String fname, String lname, String password, Date dob,
			String email, int phNumber, String state, String insPlan) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.password = password;
		this.dob = dob;
		this.email = email;
		this.PhNumber = phNumber;
		this.state = state;
		this.insPlan = insPlan;
	}

	public Patient() {
		super();
	}

}
