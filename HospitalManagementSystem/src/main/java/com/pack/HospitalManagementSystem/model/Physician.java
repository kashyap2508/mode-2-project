package com.pack.HospitalManagementSystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "physician")
public class Physician {
	@Id
	private String id;
	private String fname;
	private String lname;
	private String department;
	private String education;
	private int experience;
	private String state;
	private String insPlan;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getInsPlan() {
		return insPlan;
	}

	public void setInsPlan(String insPlan) {
		this.insPlan = insPlan;
	}

	@Override
	public String toString() {
		return "Physician [id=" + id + ", fname=" + fname + ", lname=" + lname
				+ ", department=" + department + ", education=" + education
				+ ", experience=" + experience + ", state=" + state
				+ ", insPlan=" + insPlan + "]";
	}

	public Physician(String id, String fname, String lname, String department,
			String education, int experience, String state, String insPlan) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.department = department;
		this.education = education;
		this.experience = experience;
		this.state = state;
		this.insPlan = insPlan;
	}

	public Physician(String fname, String lname, String department,
			String education, int experience, String state, String insPlan) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.department = department;
		this.education = education;
		this.experience = experience;
		this.state = state;
		this.insPlan = insPlan;
	}

	public Physician() {
		super();
	}

}
