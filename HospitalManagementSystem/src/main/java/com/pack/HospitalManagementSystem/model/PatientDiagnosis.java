package com.pack.HospitalManagementSystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "patientdiagnosis")
public class PatientDiagnosis implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int diagnosisId;
	private String symptoms;
	private String diagnosisProvided;
	private String administeredBy;
	private Date dateOfDiagnosis;
	private String isFollowUp;
	private Date dateOfFollowup;
	private double billAmount;
	private long cardNumber;
	private int patientId;
	private String mode;

	public int getDiagnosisId() {
		return diagnosisId;
	}

	public void setDiagnosisId(int diagnosisId) {
		this.diagnosisId = diagnosisId;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getDiagnosisProvided() {
		return diagnosisProvided;
	}

	public void setDiagnosisProvided(String diagnosisProvided) {
		this.diagnosisProvided = diagnosisProvided;
	}

	public String getAdministeredBy() {
		return administeredBy;
	}

	public void setAdministeredBy(String administeredBy) {
		this.administeredBy = administeredBy;
	}

	public Date getDateOfDiagnosis() {
		return dateOfDiagnosis;
	}

	public void setDateOfDiagnosis(Date dateOfDiagnosis) {
		this.dateOfDiagnosis = dateOfDiagnosis;
	}

	public String getIsFollowUp() {
		return isFollowUp;
	}

	public void setIsFollowUp(String isFollowUp) {
		this.isFollowUp = isFollowUp;
	}

	public Date getDateOfFollowup() {
		return dateOfFollowup;
	}

	public void setDateOfFollowup(Date dateOfFollowup) {
		this.dateOfFollowup = dateOfFollowup;
	}

	public double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}

	public long getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	@Override
	public String toString() {
		return "PatientDiagnosis [diagnosisId=" + diagnosisId + ", symptoms="
				+ symptoms + ", diagnosisProvided=" + diagnosisProvided
				+ ", administeredBy=" + administeredBy + ", dateOfDiagnosis="
				+ dateOfDiagnosis + ", isFollowUp=" + isFollowUp
				+ ", dateOfFollowup=" + dateOfFollowup + ", billAmount="
				+ billAmount + ", cardNumber=" + cardNumber + ", patientId="
				+ patientId + ", mode=" + mode + "]";
	}

	public PatientDiagnosis(int diagnosisId, String symptoms,
			String diagnosisProvided, String administeredBy,
			Date dateOfDiagnosis, String isFollowUp, Date dateOfFollowup,
			double billAmount, long cardNumber, int patientId, String mode) {
		super();
		this.diagnosisId = diagnosisId;
		this.symptoms = symptoms;
		this.diagnosisProvided = diagnosisProvided;
		this.administeredBy = administeredBy;
		this.dateOfDiagnosis = dateOfDiagnosis;
		this.isFollowUp = isFollowUp;
		this.dateOfFollowup = dateOfFollowup;
		this.billAmount = billAmount;
		this.cardNumber = cardNumber;
		this.patientId = patientId;
		this.mode = mode;
	}

	public PatientDiagnosis(String symptoms, String diagnosisProvided,
			String administeredBy, Date dateOfDiagnosis, String isFollowUp,
			Date dateOfFollowup, double billAmount, long cardNumber,
			int patientId, String mode) {
		super();
		this.symptoms = symptoms;
		this.diagnosisProvided = diagnosisProvided;
		this.administeredBy = administeredBy;
		this.dateOfDiagnosis = dateOfDiagnosis;
		this.isFollowUp = isFollowUp;
		this.dateOfFollowup = dateOfFollowup;
		this.billAmount = billAmount;
		this.cardNumber = cardNumber;
		this.patientId = patientId;
		this.mode = mode;
	}

	public PatientDiagnosis() {
		super();
	}

}
